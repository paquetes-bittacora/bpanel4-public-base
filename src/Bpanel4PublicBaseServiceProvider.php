<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\PublicBase;

use Bittacora\Bpanel4\PublicBase\Commands\InstallCommand;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;

final class Bpanel4PublicBaseServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->publishes(
            [__DIR__ . '/../resources/views' => Config::get('view.paths')[0] . '/bpanel4-public'],
            'bpanel4-public-base-views',
        );

        $this->commands([InstallCommand::class]);
    }
}
