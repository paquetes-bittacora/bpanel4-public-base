# bPanel 4 public base

Este paquete publica unas vistas a `resources/views/bpanel4-public` que pueden usarse como base en los paquetes que 
tengan vistas en la parte pública. Así podremos modificar la apariencia del proyecto sin tener que modificar los paquetes que crean vistas públicas.

Este paquete no tiene ningún diseño como tal, simplemente una estructura básica y unas vistas "comunes" para todos los proyectos
que hacen que no haya que modificar ningún paquete a la hora de crear la parte pública.

Las vistas que se publican son:

```
resources/views/bpanel4-public
├── layouts
│   ├── base.blade.php // No usar directamente
│   ├── fullwidth.blade.php // Para páginas de ancho completo
│   └── regular-page.blade.php // Para páginas de ancho fijo (por ejemplo)
├── mail
│   └── base-mail.blade.php // Diseño básico de los emails
└── partials 
    ├── flash-alerts.blade.php // Para las alertas tipo alert-success, etc
    ├── footer.blade.php 
    └── header.blade.php
```

Se pueden extender con `@extends('bpanel4-public.layouts.regular-page')` (por ejemplo).

## Ejemplo

Imagina por ejemplo que estás haciendo un módulo que tiene que mostrar algo en la parte pública. Entonces, creamos una
vista dentro del paquete como es habitual (`paquete-ejemplo/resources/views/ejemplo.blade.php`), con la estructura siguiente:

```html
@extends('bpanel4-public.layouts.regular-page')

@section('title') Título de la página @endsection

@section('content')
<!-- Aquí dentro ponemos lo que queramos, este html solo es un ejemplo, no hay -->
<!-- ninguna restricción, no tiene que ser un div único ni nada                -->

<div>
    <h1>Vista del paquete de ejemplo</h1>
    <p>Esta vista muestra...</p> 
</div>
@endsection
```

Como estamos extendiendo `bpanel4-public.layouts.regular-page`, que es una vista con un nombre común para todos los proyectos,
y que además está publicada en resources/views/bpanel4-public, cualquier proyecto podrá modificar la apariencia de la vista de nuestro
paquete sin modificarlo, ya que estamos inyectando el HTML propio del paquete en una vista que es genérica.

Lógicamente, lo fácil o difícil que sea modificar la apariencia dependerá mucho de como se maquete la vista del paquete, pero por
lo menos ya no hay que modificar el paquete para cambiar los `@extends` para que hagan referencia a nuestras vistas.
