@extends('bpanel4-public.layouts.base')

@section('body')
    <div class="regular-page-container">
        @yield('content')
    </div>
@endsection
