<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\PublicBase\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

final class InstallCommand extends Command
{
    protected $signature = 'bpanel4-public-base:install';

    protected $description = 'Instala el paquete para preparar la base de la parte pública de bPanel 4.';

    public function handle(): void
    {
        Artisan::call('vendor:publish', ['--tag' => 'bpanel4-public-base-views']);

        // Para adaptar algunas vistas antiguas a las rutas nuevas, creo un archivo que hace de "alias"
        if(!is_dir(base_path() . '/resources/views/bpanel/')) {
            mkdir(base_path() . '/resources/views/bpanel/');
        }
        
        if(!is_dir(base_path() . '/resources/views/bpanel/layouts/')) {
            mkdir(base_path() . '/resources/views/bpanel/layouts/');
        }

        file_put_contents(base_path() . '/resources/views/bpanel/layouts/bpanel-app.blade.php', "{{-- Este template solo sirve para adaptar algunas vistas que usan bpanel.layouts.bpanel-app en vez de bpanel4::layouts.bpanel-app --}}
@include('bpanel4::layouts.bpanel-app')");
    }
}
